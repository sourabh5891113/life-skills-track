# Event sourcing architecture

In Event sourcing, instead of storing the current state of data, we use an append-only store to store each state change of the entity that we are monitoring like orders in a cart. A state change is stored in the event store which stores it as a sequence of events and then customers can subscribe to those events and perform certain action.

### Example

In an e-commerce site when an order is added to a cart, the order service adds the order to the order table and also to the event store and it is stored as a sequence of events like order created, order approved, order shipped, etc.

![Event sourcing example](https://eventuate.io/i/storingevents.png)

In the Create, Read, Update, Delete(CRUD) model a typical data process is to read data from the store, make some modifications to it, and update the current state of the data with the new values—often by using transactions that lock the data. These operations can slow your system due to the processing overhead it requires. But in event sourcing there is no need for that because everything is happening in a sequence of events. we can also get to the current state by using the event log maintained in event sourcing.

### Benefits of Event Sourcing
* __Easy to Audit:-__ Since all the changes are captured as events we have the whole history on our hands, so we can easily audit and see our previous state changes.
* __Flexibility and Scalability:-__  Events are append-only and immutable, making it easier to scale and evolve the system over time.
* __Debugging and Diagnostics:-__ The event log provides a detailed record of all actions taken within the system, which can be used for debugging and diagnosing issues.

### References

[Pattern: Event sourcing](https://microservices.io/patterns/data/event-sourcing.html)

[Event Sourcing pattern by Microsoft](https://learn.microsoft.com/en-us/azure/architecture/patterns/event-sourcing)

[Event Sourcing Explained](https://www.youtube.com/watch?v=yFjzGRb8NOk)
