## Question 1

### Paraphrase (summarize) the [video]( https://www.youtube.com/watch?v=H14bBuluwB8) in a few (1 or 2) lines. Use your own words.

## Answer

This video talks about grit. This video explains that a significant predictor of success is not social intelligence, physical health, good looks, or IQ, instead, it is grit. Grit is passion and perseverance for very long-term goals. Grit is sticking with the future day in and day out.

## Question 2

### Paraphrase (summarize) the [video](https://www.youtube.com/watch?v=75GFzikmRY0) in a few (1 or 2) lines in your own words.

## Answer

This video talks about two types of mindsets i.e. Fixed and growth mindset. Fixed mindset people believe that skills and intelligence are set and you either have them or not they don't believe that they can be good at something they don't know or know very little. On the other hand, with a growth mindset people believe that they are in control of their abilities and they believe that skill and intelligence can be built.

## Question 3

### What is the Internal Locus of Control? What is the key point in the [video](https://www.youtube.com/watch?v=8ZhoeSaPF-k)?

## Answer

Locus of Control is the degree to which you believe you have control over your life. Internal Locus of Control means believing that the factors we control lead to good outcomes. It was our extra work and extra effort that allowed us to do well. Internal Locus of Control is the key to staying motivated in our life.


## Question 4

### What are the key points mentioned by speaker to build growth mindset (explanation not needed).

## Answer

- Believe in your ability to figure things out
- Question your assumptions
- Develop your life curriculum
- Honour the struggle

## Question 5

### What are your ideas to take action and build Growth Mindset?

## Answer

- Always believe that no task is undoable and work on every hard difficulty you face.
- Focus on the process of getting better.
- More Effort means more learning.
- Embrace every challenge and persevere through them.
- See every mistake as an opportunity to learn new things
- Appreciate every feedback and use them to get better.