## Question 1

### What are the steps/strategies to do Active Listening? (Minimum 6 points)

## Answer

Active Listening:- The act of fully hearing and comprehending the meaning of what someone else is saying. Steps to do it:-

- Focus on the speaker, not on your thoughts
- Try not to interrupt the speaker
- Use door openers and keep the speaker talking
- Use body language
- Take notes during important conversations
- Paraphrase what others have said to see that you both are on the same page

## Question 2

### According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)

## Answer

- Repeating speaker words:- This means reaping what the speaker has said, using similar words so that the speaker will get to know that you are paying attention.
- Not Repeating the same:- Instead of reaping the exact speaker's words, express the speaker's message in your own words demonstrating active listening.
- Summarizing:- Pulling together the main idea of the conversation, helps in clarity and shows that you are following closely.
- Acknowledging feeling:- this involves reflecting on the speaker's feelings and acknowledging the emotions behind the speaker's words. It shows empathy and helps build connections.

## Question 3

### What are the obstacles in your listening process?

## Answer

As mentioned in active listening points one should focus on the speaker's words instead of their thoughts. I think that is one of the obstacles in my
listening process because sometimes I tend to lose focus mid conversations and go into my thoughts. And I also feel like I need to work on Reflective Listening.

## Question 4

### What can you do to improve your listening?

## Answer

I will start implementing Active and Reflective Listening in my daily conversations and that will improve my listening. There were also other points like using body language and reflecting on the speaker's feelings, I will also start improving on that.

## Question 5

### When do you switch to Passive communication style in your day to day life?

## Answer

- To avoid conflicts and disagreement and keep things in peace
- When feeling underconfident
- When talking to a new person
- When asking for help
- When helping someone else

## Question 6

### When do you switch into Aggressive communication styles in your day to day life?

## Answer

I use Aggressive communication very little in daily life. I use it to diffuse harmful situations.

## Question 7

### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

## Answer

- To avoiding confrontation, I might resort to sarcasm to express my displeasure without openly confronting the issue
- When I'm upset or angry, I might withdraw and give the silent treatment as a way to punish the other person

## Question 8

### How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life? (Watch the videos first before answering this question.)

## Answer

- Don't be too passive and let others put you down, Instead talk to them about how you are feeling.
- Instead of only taking into account only your feelings, also think about others feeling and work accordingly.
- If not comfortable about expressing your feelings start by low-stakes situations.
