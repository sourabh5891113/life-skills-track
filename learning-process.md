## Question 1

### What is the Feynman Technique? Explain in 1 line.

## Answer

Feyman Technique suggests that if you can teach someone properly whatever you have learned then that concept is clear to you. And the explanation should be in easy words and easy to understand.

## Question 2

### In this [video](https://www.youtube.com/watch?v=O96fE1E-rf8), what was the most interesting story or idea for you?

## Answer

I find the Pomodoro Technique quite interesting. In this technique, you have to take a timer and set it for 25 minutes and then work or study for 25 minutes straight without any disturbance then after 25 minutes you can do something fun for 5 to 10 minutes and then repeat this process. This will enhance our ability to have focused attention and also our ability to relax. I will surely try it out.

## Question 3

### What are active and diffused modes of thinking?

## Answer

Active or focused mode means when you are focused on something and thinking very hard and tightly about it. As the video explains when you are thinking like this there are too many barriers between your basic thought process and a new thought process. So, in the diffused mode, you will relax yourself and take a step back think again, and try to find a new perspective. In this way, there will be less barriers in your brain and you will find a new perspective.

## Question 4

### According to the [video](https://www.youtube.com/watch?v=5MgBikgcWnY), what are the steps to take when approaching a new topic? Only mention the points.

## Answer

Steps to take when approaching a new topic/skill:-

- Deconstruct the topic/skill
- Learn enough to self-correct
- Remove practice barriers
- Practice at least 20 hours

## Question 5

### What are some of the actions you can take going forward to improve your learning process?

## Answer

- Try Pomodoro Technique
- Try diffused modes of thinking to get a better perspective
- Try teaching someone the things I learned.
- Try learning something in 20 hours.
- Sleep for 9 hours.
