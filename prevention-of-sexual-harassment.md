## Question 1

### What kinds of behaviour cause sexual harassment?

## Answer

Sexual harassment encompasses a range of behaviors, including:

- Unwanted sexual advances or propositions
- Verbal harassment, such as sexually suggestive comments, jokes
- Physical harassment, such as unwanted touching, kissing, or assault
- Displaying or sharing sexually explicit materials
- Making offensive gestures or facial expressions of a sexual nature
- Persistent or unwanted attention, such as stalking or following or asking out multiple times.
- Creating a hostile or intimidating work environment based on someone's sex or gender

These behaviors can occur in various contexts, including the workplace, educational institutions, or other settings, and can be perpetrated by individuals of any gender against individuals of any gender.

## Question 2

### What would you do in case you face or witness any incident or repeated incidents of such behaviour?

## Answer

If I faced or witnessed any incident or repeated incidents of such behavior, here's what I would do:

- **Document**:- Record the details about the incident like date, time, and what happened.
- **Report**:- Report the behavior to the appropriate authority within the organization, such as a supervisor, or HR department.
- **Support**:- Offer support to the person experiencing harassment, tell them everything will be alright, and encourage them to report the incident to the appropriate authority.
- **Follow Procedures**: Follow the organization's procedures for reporting and addressing sexual harassment.
- **Cooperate**:- Cooperate fully with any investigation that may follow, providing truthful and accurate information to assist in resolving the situation.
