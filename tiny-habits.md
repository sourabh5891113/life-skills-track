## 1) In this [video](https://www.youtube.com/watch?v=AdKUJxjn-R8), what was the most interesting story or idea for you?

In this video, BJ Fogg had a really cool idea. Instead of going all out with huge goals, he says start super small with easy habits. These tiny habits are almost too easy to resist, but over time they can blossom into big changes!

## 2) How can you use B = MAP to make making new habits easier? What are M, A and P.

According to the video, B = MAP is a formula created by BJ Fogg to explain what factors contribute to a person forming a habit. B stands for Behavior equals Motivation Ability Prompt.

- Motivation: This refers to your desire to do a particular behavior. For example, if you want to read more, you would need to have some motivation to pick up a book.
- Ability: This refers to your capacity to perform a behavior. This includes things like the time and resources required to do the behavior. Reading a book before bed requires less ability than going for a run, because reading a book requires less time and physical exertion.
- Prompt: This refers to the cue or trigger that reminds you to do the behavior. There are three types of prompts: external prompts (like phone notifications), internal prompts (like thoughts or feelings), and action prompts (completing one behavior that triggers another behavior). According to BJ Fogg, action prompts are the most effective because they are less likely to be ignored.

In short, if you want to make it easier to form a new habit, you should try to increase your motivation, ability, and prompt to do the behavior.

## 3) Why it is important to "Shine" or Celebrate after each successful completion of habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)

According to the [video](https://www.youtube.com/watch?v=S_8e-6ZHKLs), celebrating after completing a tiny habit is important because it increases your motivation and makes you more likely to repeat the habit. The speaker refers to this feeling of celebration as "shine". According to the video, celebrating even small wins is more important than the size of the win itself. This is because the frequent feeling of success builds momentum and makes you more likely to stick with the habit. The video uses the analogy of a dog trainer to explain this concept. The dog trainer gives the dog a treat after it performs a trick, even a simple trick. This positive reinforcement encourages the dog to keep performing the trick. Similarly, celebrating your tiny habits reinforces the behavior and makes you more likely to repeat it. In the video, the speaker says that celebrating tiny wins is the most critical component of habit development. So, if you want to form a new habit, don't forget to shine after you complete it!

## 4) In this [video](https://www.youtube.com/watch?v=mNeXuCYiE0U), what was the most interesting story or idea for you?

According to the video, the most interesting idea to me is the concept of aggregation of marginal gains. This is the idea that small improvements can lead to big results over time. The speaker uses the example of British Cycling to illustrate this point. British Cycling was a mediocre team for many years, but they were able to turn things around by making a lot of small improvements. For example, they started using lighter tires and more ergonomic seats. They even figured out the type of pillow that led to the best night's sleep for each rider! As a result of these small changes, British Cycling went on to win four out of the next five Tour de France titles.

## 5) What is the book's("Atomic Habits" by James Clear) perspective about Identity?

The book "Atomic Habits" by James Clear emphasizes focusing on who you want to be rather than just what you want to do.

Here's the breakdown:

- **Goals are temporary:** They focus on achieving a specific outcome, and once achieved, motivation can drop.
- **Identity is lasting:** The book argues that building habits that align with your desired identity (e.g., "I am a healthy person" or "I am a hard worker") leads to long-term change.

So, instead of just setting a goal to "exercise more," you might adopt habits that make you someone who views themselves as "healthy." This could involve small things like putting on your workout clothes every morning, even if you don't always end up exercising.

## 6) Write about the book's perspective on how to make a habit easier to do.

According to the [video](https://www.youtube.com/watch?v=YT7tQzmGRLA), the book "Atomic Habits" by James Clear says that to make a habit easier to do, you should focus on creating systems and building your identity around the habit rather than just setting goals.

Here are some key points:

- **Small changes add up:** The book argues that even small improvements can lead to big results over time. The key is to be consistent and patient.
- **Focus on systems:** Goals can be discouraging if not achieved, so the book suggests focusing on systems that help you develop the habits you need to reach your goals.
- **Identity is key:** If you see yourself as someone who does a particular behavior, you are more likely to do it. For example, if you view yourself as a healthy person, you are more likely to make healthy choices.

The book also talks about four ways to make habits easier to stick with:

- Make it obvious: Design your environment to make cues for your desired habits obvious.
- Make it attractive: Make the habits you want to develop attractive by associating them with a reward.
- Make it easy: Reduce the friction associated with good habits and make them easier to do.
- Make it satisfying: Make sure you get an immediate reward for doing good habits.

By following these tips, you can develop good habits and break bad ones.

## 7) Write about the book's perspective on how to make a habit harder to do.

The book says to make the bad habits harder to do by making the cues invisible, the action unattractive and hard, and the reward unsatisfying in an ideal world. And to make your desired behaviors easier to do and harder to resist. For instance, you could keep your workout clothes by your bed so that you are more likely to put them on in the morning. You could also get rid of unhealthy snacks from your house so that you are not tempted to eat them.

## 8) Pick one habit that you would like to do more of. What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

One habit I'd love to build is reading more!

Here's how I could make it stick based on the ideas from "Atomic Habits":

- **Obvious Cue:** My phone might be my downfall here, but I can use it for good! Setting a daily alarm for "Reading Time" at a consistent hour would be a clear reminder to pick up a book.
- **Attractive Habit:** Reading can be fun, but sometimes it loses its appeal. To make it more attractive, I could reward myself by eating healthy snacks or watching something that interests me like football.
- **Easy Action:** Preparation is key! I could set aside some interesting articles or book chapters I've been wanting to explore and keep them readily available on my phone or tablet. That way, whenever the reading alarm goes off, I have engaging material at my fingertips.
- **Satisfying Reward:** Reading itself can be rewarding, especially when you stumble upon something fascinating. But to solidify the habit, I could track my reading progress in a simple journal. Hitting milestones, like finishing a book or reading for a full week straight, could be mini-celebrations that keep me motivated.

By incorporating these steps, I believe I can create a reading habit that sticks. More reading means a sharper mind, a broader perspective.

## 9) Pick one habit that you would like to eliminate or do less of. What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

One behavior I would like to reduce is spending excessive time engaged in unproductive phone scrolling. This mindless activity can significantly diminish the time available for more valuable things. Here's a potential strategy to address this:

- **Reduce the Cue:** Disabling unnecessary phone notifications except for urgent calls and messages would minimize the prompts that trigger unintentional scrolling sessions.
- **Decrease the Appeal:** Replacing time-consuming phone applications with those that promote positive behaviors could make scrolling a less attractive option. Examples include fitness trackers or language learning applications.
- **Increase the Difficulty:** Employing silent mode and keeping the phone out of sight during designated focus periods, such as work hours or dedicated reading time, can add an obstacle to mindless phone use.
