## 1) What are the activities you do that make you relax - Calm quadrant?

Here are some activities that can help me relax and move into the Calm quadrant:

- Meditation: helps in calming your mind and reducing stress.
- Yoga: Engage in gentle yoga poses to relax your body and mind.
- Nature walks: Spend time outdoors surrounded by nature to unwind and relax.
- Reading: Escape into a good book to distract yourself from stressors.
- Listening to calming music: Play soothing music to relax and ease tension.
- Taking a warm bath: Enjoy a relaxing soak in the tub to release stress and muscle tension.
- Engaging in hobbies: Pursue activities you enjoy, such as working out, playing football, or cooking, to unwind and recharge.

## 2) When do you find getting into the Stress quadrant?

Here are some common situations where I may find myself entering the Stress quadrant:

- Tight deadlines at work.
- Facing challenging situations
- Financial worries
- Relationship conflicts
- Health issues
- Major life changes
- Lack of control
- Uncertainty

## 3) How do you understand if you are in the Excitement quadrant?

Here are some signs that indicate that I might be in the Excitement quadrant:

- Feeling energized and enthusiastic about upcoming events or opportunities.
- Feeling motivated and ready to take action.
- Experiencing a surge of creativity or inspiration.
- Feeling a sense of accomplishment and satisfaction.
- Being fully engaged and focused on the present moment.
- Experiencing heightened physical sensations, such as increased heart rate or adrenaline.
- Feeling a sense of adventure and willingness to try new things.
- Experiencing moments of joy, happiness, or exhilaration.

## 4) Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.

This video talks about the importance of sleep for our health.

The speaker, a sleep scientist, says sleep is not a choice but a necessity. Enough sleep is important for our brain, immune system, and overall health. Here are some key points:

- Lack of sleep harms memory and learning.
- Sleep deprivation increases the risk of diseases.
- Short sleep is linked to early death.
- To improve sleep, go to bed and wake up at consistent times and keep your bedroom cool.

## 5) What are some ideas that you can implement to sleep better?

here are some ideas to sleep better:

- Go to bed and wake up at the same time each day, even on weekends.
- Keep your bedroom cool, around 65 degrees Fahrenheit (18 degrees Celsius).
- Avoid napping during the day. Naps can interfere with your nighttime sleep.
- Avoid alcohol and caffeine in the hours leading up to bedtime.
- If you're in bed and can't fall asleep after 20 minutes, get out of bed and go to a different room. Do something relaxing until you feel tired.

## 6) Paraphrase the [video](https://www.youtube.com/watch?v=BHY0FxzoKZE) - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.

1. Exercise is the most transformative thing you can do for your brain. It has immediate, long-lasting, and protective benefits.
2. Exercise increases the levels of neurotransmitters like dopamine, serotonin, and noradrenaline, which improves mood, focus, attention, and reaction times.
3. Exercise increases the size of the hippocampus, which is critical for long-term memory.
4. Exercise protects the brain from neurodegenerative diseases like Alzheimer's disease and dementia.
5. The minimum amount of exercise recommended is 3-4 times a week for 30 minutes each time. You can get aerobic exercise by walking, taking the stairs, or going to the gym.

## 7) What are some steps you can take to exercise more?

- Aim for at least 30 minutes of aerobic exercise three to four times a week. This could include brisk walking, taking the stairs, or power vacuuming.
- You can find ways to incorporate exercise into your daily routine. For example, take a walk during your lunch break or use the stairs instead of the lift.
- Find an exercise routine that you enjoy. So that the exercise becomes fun to do and enjoyable.