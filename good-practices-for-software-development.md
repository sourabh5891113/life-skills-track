## 1) Which point(s) were new to you?

Points that were new to me:-

- **Gathering requirements:**

  - Take notes during requirement discussions.
  - Share documented requirements for immediate feedback.
  - Seek clarity and ask questions to avoid any confusion.

- **Over-communication:**

  - Inform team about changes in requirements and deadlines.
  - Notify about technical issues affecting work.
  - Utilize group chats for communication over private messages.
  - Always respond to calls promptly.

- **Asking questions:**

  - Clearly explain the problem.
  - Mention attempted solutions.
  - Use visuals like screenshots and diagrams.
  - Share code snippets using platforms like GitHub gists.

- **Getting to know teammates:**

  - Join meetings early to interact with team members.
  - Be mindful of their schedules when setting up calls.

- **Being aware and mindful:**

  - Consolidate questions into single messages.
  - Respond promptly to messages.
  - Respect team members' time and workload.

- **Working with involvement:**
  - Maintain focus during remote work.
  - Practice deep work for sustained attention.
  - Limit distractions like social media.
  - Manage time and productivity with tracking apps.
  - Maintain a healthy lifestyle with proper nutrition and exercise.

## 2) Which area do you think you need to improve on? What are your ideas to make progress in that area?

The area I think I need to improve is Time management and productivity. Here are some ideas to make progress in this area:-

- Utilize time tracking apps like Boosted to monitor productivity.
- Implement techniques from deep work methodology to enhance focus.
- Minimize distractions by blocking social media during work hours.
- Establish a consistent work-play balance to maintain productivity.
- Incorporate regular breaks and exercise to sustain energy levels throughout the day.
